<?php
    require('function.php');
    $db = connectDB();
?>

<html>
    <head>
        <title>Дашборды</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../../css/styles.css">
    </head>
    <body>
        <!-- боковая панель -->
        <aside class="aside">

            <div class="cls-btn">
                <span class="cls-btn__text">Закрыть</span>
            </div>
          
            <div class="logo"></div>
          
            <nav class="nav">
          
                <a class="nav__item nav__item_sales" routerLinkActive="nav__item_active" href="/dashboard/selling">
          
                    <div class="nav__item-tooltip">Продажи</div>
          
                </a>
          
                <a class="nav__item nav__item_structure" routerLinkActive="nav__item_active" href="/dashboard/structure">
          
                    <div class="nav__item-tooltip">Структура продаж</div>
          
                </a>

                <a class="nav__item nav__item_romb" routerLinkActive="nav__item_active" href="/dashboard/romb">
          
                    <div class="nav__item-tooltip">Ромб</div>
          
                </a>
          
                <a class="nav__item nav__item_log-out">
          
                    <div class="nav__item-tooltip">Выйти</div>
          
                </a>
          
            </nav>
          
        </aside>

        <!-- вернихний блок с кнопками -->

        <section class="header_controlls">
                <div class="radio-btn-group">

                    <label class="filtration__item-wrapper">

                        <input class="filtration__item-btn"
                            type="radio">
                              
                        <div class="filtration__item-visualization">
                            Месяц
                        </div>
                              
                    </label>
                    <label class="filtration__item-wrapper">

                        <input class="filtration__item-btn"
                            type="radio">
                                  
                        <div class="filtration__item-visualization">
                            Квартал
                        </div>
                                  
                    </label>
                    <label class="filtration__item-wrapper">

                        <input class="filtration__item-btn"
                            type="radio">
                                      
                        <div class="filtration__item-visualization">
                            Год
                        </div>
                                      
                    </label>

                    <div class="calendar__btn">
                        <div class="calendar__btn-val"> 20/09/2018 </div>
                    </div>

                    <div class="window-select">Локация: Волгоград, Волжский, Камы...</div>         

                </div>
        </section>

        <div class="centr">
            <section class="snippet-left">
                <div class="snippet-left__capacity">
                    <div class="snippet-left__title">Производительность</div>
                    <div class="snippet-left__capacity__indicator"></div>
                    <span class="snippet-left__capacity__speed">Высокая</span>
                    <span class="snippet-left__capacity__rate">422 ч/с</span>
                </div>

                <div class="snippet-left__product">
                    <div class="snippet-left__title">Продукт</div>
                    <span class="snippet-left__product__procent">100%</span>
                    <span class="snippet-left__product__address">0 обращения</span>
                </div>

                <div class="snippet-left__product">
                    <div class="snippet-left__title">Сервис</div>
                    <span class="snippet-left__product__procent">100%</span>
                    <span class="snippet-left__product__address">0 обращения</span>
                </div>
                <div class="snippet-left__delivery">
                    <div class="snippet-left__wrapper">
                        <div class="snippet-left__title">Скорость доставки</div>
                        <span class="snippet-left__product__procent">100%</span>
                        <span class="snippet-left__delivery__time">00:58:00</span>
                    </div>
                    <div class="snippet-left__wrapper">
                        <div class="snippet-left__title">Опоздание курьера</div>
                        <span class="snippet-left__product__procent">100%</span>
                        <span class="snippet-left__delivery__time">00:58:00</span>
                    </div>
                </div>
                <div class="snippet-left__wrap-speed">
                    <div class="snippet-left__delivery delivery">
                        <div class="snippet-left__wrapper">
                            <div class="snippet-left__title">Скорость со стола</div>
                            <span class="snippet-left__product__procent">100%</span>
                            <span class="snippet-left__delivery__time">00:58:00</span>
                        </div>
                        <div class="snippet-left__wrapper">
                            <div class="snippet-left__title">Опоздание по вине ТО</div>
                            <span class="snippet-left__product__procent">100%</span>
                            <span class="snippet-left__delivery__time">00:58:00</span>
                        </div>
                    </div>
                    <div class="snippet-left__delivery delivery">
                        <div class="snippet-left__wrapper">
                            <div class="snippet-left__title">Япония</div>
                            <span class="snippet-left__product__procent">100%</span>
                            <span class="snippet-left__delivery__time">00:58:00</span>
                        </div>
                        <div class="snippet-left__wrapper">
                            <div class="snippet-left__title">Паназия</div>
                            <span class="snippet-left__product__procent">100%</span>
                            <span class="snippet-left__delivery__time">00:58:00</span>
                        </div>
                    </div>
                </div>
            </section>

            <section class="schedule">
                <!--<div class="schedule__img"></div>-->
                <div class="schedule__img_canvas">
                    <canvas id="canvas"></canvas>
                </div>
                <div class="schedule__buttons">
                    <label class="schedule__buttons_label" data-index="0">
                        <input type="checkbox" class="schedule__buttons__input">
                        <div class="schedule__buttons__item">
                            <div class="schedule__buttons__item-indicator"></div>
                            <div class="schedule__buttons__item-text">Итого</div>
                        </div>
                    </label>
                    <?php
                    $points = get_listPoint($db);
                    foreach($points as $point){
                    ?>
                    <label class="schedule__buttons_label" data-index="<?=$point['id']?>">
                        <input type="checkbox" class="schedule__buttons__input">
                        <div class="schedule__buttons__item">
                            <div class="schedule__buttons__item-indicator"></div>
                            <div class="schedule__buttons__item-text"><?=$point['name']?></div>
                        </div>
                    </label>
                    <?php } ?>
                </div>
            </section>
        </div>


        <!-- таблица производительности -->

        <section class="table-product">
            <div class="table-product__header">
                <div class="table-product__header__item item-1 city">Город</div>
                <label>
                    <input type="checkbox" class="table-product__header__input">
                    <div class="table-product__header__item productivity">Производительность</div>
                </label>
                <label>
                    <input type="checkbox" class="table-product__header__input">
                    <div class="table-product__header__item product">Продукт</div>
                </label>
                <label>
                    <input type="checkbox" class="table-product__header__input">
                    <div class="table-product__header__item service">Сервис</div>
                </label>
                <label>
                    <input type="checkbox" class="table-product__header__input">
                    <div class="table-product__header__item delivery">Скорость доставки</div>
                </label>
                <label>
                    <input type="checkbox" class="table-product__header__input">
                    <div class="table-product__header__item desk">Скорость со стола</div>
                </label>
            </div>

            <ul class="table-product__list">
                <?php $total = get_totalResult($db);?>
                <li class="table-product__list__item" data-index="0">
                    <label class="table-product__list__label">
                        <input type="checkbox" class="table-product__list__input" checked>
                        <div class="table-product__list__wrap">
                            <div class="table-product__list__border"></div>
                            <div class="table-product__list__checked">
                                <img src="/assets/img/select/tick.svg" alt="" class="table-product__list__tick">
                            </div>
                        </div>
                    </label>
                    <div class="table-product__list__text">Итого</div>
                    <div class="table-product__list__wrapper productivity">
                        <div class="table-product__list__wrapper-header">
                            <div class="table-product__list__indicator"></div>
                            <span class="table-product__list__wrapper-text">Высокая</span>
                        </div>
                        <div class="table-product__list__rate table-product__list__rate-m">422 ч/с</div>
                    </div>
                    <div class="table-product__list__wrapper product">
                        <div class="table-product__list__wrapper-header">
                            <span class="table-product__list__wrapper-text"><?=$total['product']?>%</span>
                        </div>
                        <div class="table-product__list__rate">5%
                            <span class="table-product__list__wrapper-time">опоздания</span>
                        </div>
                    </div>
                    <div class="table-product__list__wrapper service">
                        <div class="table-product__list__wrapper-header">
                            <span class="table-product__list__wrapper-text"><?=$total['service']?>%</span>
                        </div>
                        <div class="table-product__list__rate">5%
                            <span class="table-product__list__wrapper-time">опоздания</span>
                        </div>
                    </div>
                    <div class="table-product__list__wrapper delivery">
                        <div class="table-product__list__wrapper-header">
                            <span class="table-product__list__wrapper-text"><?=$total['speed']?>%</span>
                            <span class="table-product__list__wrapper-time">00:15:00</span>
                        </div>
                        <div class="table-product__list__rate">5%
                            <span class="table-product__list__wrapper-time">опоздания</span>
                        </div>
                    </div>
                    <div class="table-product__list__wrapper desk">
                        <div class="table-product__list__wrapper-header">
                            <div class="table-product__list__indicator"></div>
                            <span class="table-product__list__wrapper-text"><?=$total['quality']?>%</span>
                            <span class="table-product__list__wrapper-time">00:15:00</span>
                        </div>
                        <div class="table-product__list__rate table-product__list__rate-m">5%
                            <span class="table-product__list__wrapper-time">опоздания</span>
                        </div>
                    </div>
                </li>

                <?php
                    $points = get_listPoint($db);
                    foreach($points as $point){
                ?>
                <li class="table-product__list__item" data-index="<?=$point['id']?>">
                    <label class="table-product__list__label">
                        <input type="checkbox" class="table-product__list__input">
                        <div class="table-product__list__wrap">
                            <div class="table-product__list__border"></div>
                            <div class="table-product__list__checked">
                                <img src="/assets/img/select/tick.svg" alt="" class="table-product__list__tick">
                            </div>
                        </div>
                    </label>
                    <div class="table-product__list__text"><?=$point['name']?></div>
                    <div class="table-product__list__wrapper productivity">
                        <div class="table-product__list__wrapper-header">
                            <div class="table-product__list__indicator"></div>
                            <span class="table-product__list__wrapper-text">Высокая</span>
                        </div>
                        <div class="table-product__list__rate table-product__list__rate-m">422 ч/с</div>
                    </div>
                    <div class="table-product__list__wrapper product">
                        <div class="table-product__list__wrapper-header">
                            <span class="table-product__list__wrapper-text"><?=$point['product']?>%</span>
                        </div>
                        <div class="table-product__list__rate">5%
                            <span class="table-product__list__wrapper-time">опоздания</span>
                        </div>
                    </div>
                    <div class="table-product__list__wrapper service">
                        <div class="table-product__list__wrapper-header">
                            <span class="table-product__list__wrapper-text"><?=$point['service']?>%</span>
                        </div>
                        <div class="table-product__list__rate">5%
                            <span class="table-product__list__wrapper-time">опоздания</span>
                        </div>
                    </div>
                    <div class="table-product__list__wrapper delivery">
                        <div class="table-product__list__wrapper-header">
                            <span class="table-product__list__wrapper-text"><?=$point['speed']?>%</span>
                            <span class="table-product__list__wrapper-time">00:15:00</span>
                        </div>
                        <div class="table-product__list__rate">5%
                            <span class="table-product__list__wrapper-time">опоздания</span>
                        </div>
                    </div>
                    <div class="table-product__list__wrapper desk">
                        <div class="table-product__list__wrapper-header">
                            <div class="table-product__list__indicator"></div>
                            <span class="table-product__list__wrapper-text"><?=$point['quality']?>%</span>
                            <span class="table-product__list__wrapper-time">00:15:00</span>
                        </div>
                        <div class="table-product__list__rate table-product__list__rate-m">5%
                            <span class="table-product__list__wrapper-time">опоздания</span>
                        </div>
                    </div>
                </li>
                <?php } ?>

            </ul>
        </section>

        <!-- боковое меню локации -->

        <!-- <form class="departments">

            <div class="departments__close-bgr"></div>
              
            <div class="departments__content">
              
                <div class="search">
                    <input class="search__field" type="text">
                </div>

                <div class="select_value">
                    Сравнение: по городам
                    <div class="select_marker"></div>
                </div>
                      
                      
                <ul class="select_list">
                      
                    <li class="select_list-item">
                        по дивизионам
                    </li>
                    <li class="select_list-item">
                        по областям
                    </li>
                    <li class="select_list-item">
                        по городам
                    </li>
                    <li class="select_list-item">
                        по торговым точкам
                    </li>

                </ul>
              
                <ul class="sort-btns">
                    <li class="sort-btns__item">
                        <label class="sort-btns__item-label">
                            <input class="sort-btns__item-label-field" type="radio">
                            <div class="sort-btns__item-label-mark">
                                Действующие
                            </div>
                        </label>
                    </li>
                    <li class="sort-btns__item">
                        <label class="sort-btns__item-label">
                            <input class="sort-btns__item-label-field" type="radio">
                            <div class="sort-btns__item-label-mark">
                                Закрытые
                            </div>
                        </label>
                    </li>
                    <li class="sort-btns__item">
                        <label class="sort-btns__item-label">
                            <input class="sort-btns__item-label-field" type="radio">
                            <div class="sort-btns__item-label-mark">
                                MYBOX
                            </div>
                        </label>
                    </li>
                    <li class="sort-btns__item">
                        <label class="sort-btns__item-label">
                            <input class="sort-btns__item-label-field" type="radio">
                            <div class="sort-btns__item-label-mark">
                                Пит Point
                            </div>
                        </label>
                    </li>
                </ul>

                <div class="select_value select_checked">
                    <div class="select_marker select_checked_marker"></div>
                    MYBOX
                    <label >
                        <input type="checkbox" class="select_checked_input">
                        <div class="input__background">
                            <img src="/assets/img/select/tick.svg" alt="" class="input__background__tick">
                        </div>
                    </label>
                </div>                    
                <ul class="select_list">          
                    <li class="select_value select_checked">
                        <div class="select_marker select_checked_marker"></div>
                         Волгоград
                        <input type="checkbox" class="select_checked_input">
                        <div class="input__background">
                            <img src="/assets/img/select/tick.svg" alt="" class="input__background__tick">
                        </div>
                    </li>
                    <li class="select_value select_checked">
                        <div class="select_marker select_checked_marker"></div>
                        Волжский
                        <input type="checkbox" class="select_checked_input">
                        <div class="input__background">
                            <img src="/assets/img/select/tick.svg" alt="" class="input__background__tick">
                        </div>
                    </li>
                </ul>
                <div class="select_value select_checked">
                    <div class="select_marker select_checked_marker"></div>
                    Пит Point
                    <label >
                        <input type="checkbox" class="select_checked_input">
                        <div class="input__background">
                            <img src="/assets/img/select/tick.svg" alt="" class="input__background__tick">
                        </div>
                    </label>
                </div>                    
                <ul class="select_list">         
                    <li class="select_value select_checked">
                        <div class="select_marker select_checked_marker"></div>
                         Волгоград
                        <input type="checkbox" class="select_checked_input">
                        <div class="input__background">
                            <img src="/assets/img/select/tick.svg" alt="" class="input__background__tick">
                        </div>
                    </li>
                    <li class="select_value select_checked">
                        <div class="select_marker select_checked_marker"></div>
                        Волжский
                        <input type="checkbox" class="select_checked_input">
                        <div class="input__background">
                            <img src="/assets/img/select/tick.svg" alt="" class="input__background__tick">
                        </div>
                    </li>
                </ul>
              
                <ul class="department-controls controls">
              
                    <li class="controls__btn controls__btn_default">По умолчанию</li>
              
                    <li class="controls__btn controls__btn_ready">Готово</li>
              
                </ul>
              
            </div>
              
        </form> -->
              
          
        <script src="/js/libs/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
        <script src="/js/utils.js"></script>
        <script src="/js/dashbord.js"></script>

    </body>
</html>