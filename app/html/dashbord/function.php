<?php

//use Simplon\Mysql\Mysql;
//use Simplon\Mysql\PDOConnector;

require __DIR__ . '/../../../vendor/autoload.php';

function connectDB()
{
    $host = '172.23.0.3';
    $db = 'myboxx';
    $user = 'root';
    $pwd = 'password';

//    $host = 'localhost';
//    $db = 'orders_beta';
//    $user = 'orders';
//    $pwd = 'G&5Pp^XNu55T';

//    $pdo = new PDOConnector(
//        '172.23.0.2', // server
//        'root',      // user
//        'password',      // password
//        'myboxx'   // database
//    );

//    $pdo = new PDOConnector(
//        'localhost', // server
//        'orders',      // user
//        'G&5Pp^XNu55T',      // password
//        'orders_beta'   // database
//    );

//    $db = new Mysql($pdo->connect());
//    return $db;

    $dsn = 'mysql:host=' . $host . ';dbname=' . $db . ';charset=utf8';

    $pdo = new \Slim\PDO\Database($dsn, $user, $pwd);

    return $pdo;
}

function get_listPoint($db)
{

    $selectStatement = $db->select()->from('region_data');

    $stmt = $selectStatement->execute();
    $results = $stmt->fetchAll();
//
    return $results;
}

function get_totalResult($db){

    $query = 'SELECT 
                round(AVG(product),2) as product ,
                round(AVG(service),2) as service ,
                round(AVG(speed),2) as speed ,
                round(AVG(quality),2) as quality 
                FROM `region_data`';

    $results = $db->query($query)->fetch();

    return $results;
}