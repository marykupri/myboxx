var randomScalingFactor = function() {
    return Math.round(Math.random() * 100);
};


var colorNames = Object.keys(window.chartColors);

var row = $('.table-product__list').find('li input:checked');
var li = row.parent().parent();
var arr = [];
var tmp = '';
tmp = li.find(".product span").html().replace(/%/,'');
arr.push(tmp);
tmp = li.find(".service span").html().replace(/%/,'');
arr.push(tmp);
tmp = li.find(".delivery span").html().replace(/%/,'');
arr.push(tmp);
tmp = li.find(".desk span").html().replace(/%/,'');
arr.push(tmp);

var colorName = colorNames[0];
var newColor = window.chartColors[colorName];

var newDataset = {
    borderColor: newColor,
    backgroundColor: "transparent",
    pointBorderColor: newColor,
    data: arr,
    originIndex: 0
};

var parent = $('.table-product__list').find('li[data-index=0]');
parent.find('.table-product__list__border').css('background',newColor);
parent.find('.table-product__list__checked').css('background',newColor);

var button = $('.schedule__buttons').find('label[data-index=0]');
button.css('display','initial');
button.find('.schedule__buttons__item-indicator').css('background',newColor);

var color = Chart.helpers.color;
var config = {
    type: 'radar',
    data: {
        labels: ['Продукт', 'Сервис','Скорость со стола',['Скорость','доставки']],
        datasets: [newDataset]
    },
    options: {
        legend: {
            position: 'bottom',
            display:false
        },
        scale: {
            ticks: {
                beginAtZero: true,
                stepSize:100,
                fontSize:0
            },
            pointLabels: {
                fontSize: 14,
                fontColor: '#9b9694',
                fontFamily: "'Roboto-Regular', sans-serif",
                padding: 15
            },
        },
        tooltips: {
            mode: 'point',
            // enabled: false,
            callbacks: {
                label: function(tooltipItem, data) {
                    return tooltipItem.yLabel;
                }
            }
            // custom: function(tooltipModel) {
            //     console.log(tooltipModel);
            //     // Tooltip Element
            //     var tooltipEl = document.getElementById('chartjs-tooltip');
            //     console.log(tooltipEl);
            // }
        },
        // onClick: handleClick
    }
};

window.onload = function() {
    window.myRadar = new Chart(document.getElementById('canvas'), config);
};

function handleClick(evt) {
    var activeElement = myRadar.getElementAtEvent(evt);
    if(activeElement.length>0){
        var keepShowing = myRadar.data.keepShowing;
        console.log(keepShowing);
    //     if(keepShowing.includes(activeElement[0]._index)){
    //         var index = keepShowing.indexOf(activeElement[0]._index);
    //         keepShowing.splice(index, 1);
    //     }else{
    //         keepShowing.push(activeElement[0]._index);
    //     }
    }
};


$('.table-product__list__checked').on('click',function() {

    var li = $(this).parent().parent().parent();

    if (!li.find("label input").prop("checked")) {
        var arr = [];
        var tmp = '';
        tmp = li.find(".product span").html().replace(/%/,'');
        arr.push(tmp);
        tmp = li.find(".service span").html().replace(/%/,'');
        arr.push(tmp);
        tmp = li.find(".delivery span").html().replace(/%/,'');
        arr.push(tmp);
        tmp = li.find(".desk span").html().replace(/%/,'');
        arr.push(tmp);

        var index = li.attr('data-index');
        addDataset(arr,index);
    } else{
        // var index = $(this).parent().parent().attr('data-index');
        var index = li.attr('data-index');
        removeDataset(index);
    }
})

function removeDataset(index){

    var positiveArr = config.data.datasets.filter(function(dataset) {
        return dataset.originIndex != index;
    });

    var parent = $('.table-product__list').find('li[data-index=' + index + ']');
    parent.find('.table-product__list__border').css('background','');
    parent.find('.table-product__list__checked').css('background','');

    var button = $('.schedule__buttons').find('label[data-index=' + index + ']');
    button.css('display','none');

    console.log(positiveArr);
    config.data.datasets = positiveArr;
    window.myRadar.update();
}

function addDataset(arr,index){
    var colorName = colorNames[index];
    var newColor = window.chartColors[colorName];

    var newDataset = {
        borderColor: newColor,
        backgroundColor: "transparent",
        pointBorderColor: newColor,
        data: [],
        originIndex: index
    };

    var parent = $('.table-product__list').find('li[data-index=' + index + ']');
    parent.find('.table-product__list__border').css('background',newColor);
    parent.find('.table-product__list__checked').css('background',newColor);

    var button = $('.schedule__buttons').find('label[data-index=' + index + ']');
    button.css('display','initial');
    button.find('.schedule__buttons__item-indicator').css('background',newColor);

    $.each(arr, function(index, value){
        newDataset.data.push(value);
    });

    config.data.datasets.push(newDataset);
    window.myRadar.update();

    return config.data.datasets.length;
}