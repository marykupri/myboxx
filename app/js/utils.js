'use strict';

window.chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)',
    yellow: 'rgb(255, 205, 86)',

    cornflowerblue: 'rgb(100,149,237)',
    darkolivegreen: 'rgb(85,107,47)',
    goldenrod: 'rgb(218,165,32)',
    brown: 'rgb(165,42,42)',
    amethist: 'rgb(153,102,204)',
    atomictangerine: 'rgb(255,153,102)',
    brightube: 'rgb(209,159,232)',
    carminepink: 'rgb(235,76,66)'
};

(function(global) {

    var COLORS = [
        '#4dc9f6',
        '#f67019',
        '#537bc4',
        '#acc236',
        '#166a8f',
        '#00a950',
        '#58595b',
        '#8549ba',
        '#f53794'
    ];

    var Samples = global.Samples || (global.Samples = {});
    var Color = global.Color;

    Samples.utils = {
        // Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
        srand: function(seed) {
            this._seed = seed;
        },



        color: function(index) {
            return COLORS[index % COLORS.length];
        },
    };

    // DEPRECATED
    window.randomScalingFactor = function() {
        return Math.round(Samples.utils.rand(-500, 500));
    };

    // INITIALIZATION

    Samples.utils.srand(Date.now());


}(this));